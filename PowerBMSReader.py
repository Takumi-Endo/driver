import time
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

# Create the I2C bus
i2c = busio.I2C(board.SCL, board.SDA)

#i2c = smbus.SMBus(3)
#i2c_addr = [0x40, 0x41, 0x42, 0x44, 0x45, 0x46, 0x48, 0x49, 0x4a, 0x4c, 0x4d, 0x4e]
i2c_addr1 = [0x40, 0x41, 0x44, 0x45, 0x48, 0x49, 0x4c, 0x4d]
i2c_addr2 = [0x42, 0x46, 0x4a, 0x4e]
ch1 = len(i2c_addr1)
ch2 = len(i2c_addr2)*2
convValue1 = [22.6, 22.6, 22.6, 22.6, 22.6, 22.6, 22.6, 22.6]
convValue2 = [50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0]
buf1 = [0 for i in range(ch1)]
buf2 = [0 for i in range(ch2)]
myValue1_buf = [0 for i in range(ch1)]
myValue2_buf = [0 for i in range(ch2)]
myValue1 = [0 for i in range(ch1)]
myValue2 = [0 for i in range(ch2)]
inMax = 4
ads1 = []
ads2 = []

for i in range(ch1):
    try:
        ads1.append(ADS.ADS1115(i2c, address=i2c_addr1[i], gain=1))#2/3))
    except:# IOError
        ads1.append(-9999)

for i in range(int(ch2/2)):
    try:
        ads2.append(ADS.ADS1115(i2c, address=i2c_addr2[i], gain=2/3))
    except:# IOError:
        ads2.append(-9999)

# Create differential input between channel 0 and 1
#for i in range(ch1):
#    buf1[i] = AnalogIn(ads[i], ADS.P0, ADS.P1)
cnt = 0
for i in range(ch1):
    for j in range(int(inMax/inMax)):
        if ads1[i] == -9999:
            myValue1_buf[cnt] = -9999
        else:
            myValue1_buf[cnt] = AnalogIn(ads1[i], eval("ADS.P" + str(j*2)), eval("ADS.P" + str(j*2+1) )).voltage
        cnt += 1

cnt = 0
for i in range(int(ch2/2)):
    for j in range(int(inMax/2)):
        if ads2[i] == -9999:
            myValue2_buf[cnt] = -9999
        else:
            myValue2_buf[cnt] = AnalogIn(ads2[i], eval("ADS.P" + str(j*2)), eval("ADS.P" + str(j*2+1) )).voltage
        cnt += 1
#
#while True:
def GetValues(myValue1, myValue2):
    for i in range(ch1):
        if myValue1_buf[i] == -9999:
            myValue1[i] = -9999
        else:
            myValue1[i] = round(myValue1_buf[i] * convValue1[i], 2)


    for i in range(len(myValue2_buf)):
        if myValue2_buf[i] == -9999:
            myValue2[i] = -9999
        else:
            myValue2[i] = round(myValue2_buf[i] * convValue2[i], 2)

    #print(myValue1, end="V\t")
    #print(myValue2, end="A\n")

    #myValue1_buf.clear()
    #myValue2_buf.clear()
    
    #time.sleep(0.1)
