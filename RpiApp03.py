#coding: UTF-8
import datetime
#import threading
import signal
from multiprocessing import Value, Array, Process
import time
import math
import RPi.GPIO as GPIO
import psutil
 
import ConfigApp as config
import PulseReader as PR
import ads1x15_multiple as ADC
import TemperatureReader as TR
import DistanceReader as DST


GPIO.setmode(GPIO.BCM)                  #GPIO利用設定
GPIO.cleanup()
GPIO.setup(config.GPIO_P1, GPIO.IN)     #パルス1
GPIO.setup(config.GPIO_P2, GPIO.IN)     #パルス2
GPIO.setup(config.GPIO_P3, GPIO.IN)     #パルス3
GPIO.setup(config.GPIO_P4, GPIO.IN)     #パルス4
GPIO.setup(config.GPIO_P5, GPIO.IN)     #パルス5
GPIO.setup(config.GPIO_P6, GPIO.IN)     #パルス6
GPIO.setup(config.GPIO_P7, GPIO.IN)     #パルス7
GPIO.setup(config.GPIO_P8, GPIO.IN)     #パルス8
GPIO.setup(4, GPIO.IN)



#パルス取得スレッド定義
def StartPulseReaderThread(PR_Value, PR_Error):
    while 1:
        try:
            PR.StartPulseReader(PR_Value)
            PR_Error = False
        except OSError:
            PR_Error = True

def StartADCReaderThread(ADC_Value, ADC_Error):
    while 1:
        try:
            ADC.StartADCReader(ADC_Value)
            ADC_Error = False
        except OSError:
            ADC_Error = True

def StartTemperatureReaderThread(TR_Value, TR_Error):
    while 1:
        try:
            TR.StartTemperatureReader(TR_Value)
            TR_Error = False
        except OSError:
            TR_Error = True

def StartDistanceReaderThread(DST_Value, TR_Error):
    while 1:
        try:
            DST.StartDistanceReader(DST_Value)
            DST_Error = False
        except OSError:
            DST_Error = True

#メインルーチン
def MainThread(TR_Value, PR_Value, ADC_Value, DST_Value, TR_Error, PR_Error, ADC_Error, DST_Error):

    while True:
        if PR_Error == True:
            print('PR_Error has Occuerd')
        if ADC_Error == True:
            print('ADC_Error has Occuerd')
        if TR_Error == True:
            print('TR_Error has Occuerd')
        if DST_Error == True:
            print('DST_Error has Occuerd')
        time.sleep(100000)

    
def OutputData(arg1, arg2):

    dt_now = datetime.datetime.now()


    Time_Value = ("{0}-{1:02d}-{2:02d},{3:02d}:{4:02d}:{5:02d},{6:03d}" \
        .format(dt_now.year, dt_now.month, dt_now.day, \
                dt_now.hour, dt_now.minute, dt_now.second, \
                math.floor(dt_now.microsecond/100000)*100)) #dt_now.microsecond))
               
    TR_Value_str = [str(n) for n in TR_Value]
    [s+ ',' for s in TR_Value_str]
    
    PR_Value_str = [str(n) for n in PR_Value]
    [s+ ',' for s in PR_Value_str]

    ADC_Value_str = [str(n) for n in ADC_Value]
    [s+ ',' for s in ADC_Value_str]

    DST_Value_str = [str(n) for n in DST_Value]
    [s+ ',' for s in DST_Value_str]
    
    #if GPIO.input(4) == True:
    csvData = (Time_Value + "," \
        + TR_Value_str[0] + "," \
        + TR_Value_str[1] + "," \
        + TR_Value_str[2] + "," \
        + TR_Value_str[3] + "," \
        + PR_Value_str[0] + "," \
        + PR_Value_str[1] + "," \
        + PR_Value_str[2] + "," \
        + PR_Value_str[3] + "," \
        + PR_Value_str[4] + "," \
        + PR_Value_str[5] + "," \
        + PR_Value_str[6] + "," \
        + PR_Value_str[7] + "," \
        + ADC_Value_str[0] + "," \
        + ADC_Value_str[1] + "," \
        + ADC_Value_str[2] + "," \
        + ADC_Value_str[3] + "," \
        + ADC_Value_str[4] + "," \
        + ADC_Value_str[5] + "," \
        + ADC_Value_str[6] + "," \
        + ADC_Value_str[7] + "," \
        + ADC_Value_str[8] + "," \
        + ADC_Value_str[9] + "," \
        + ADC_Value_str[10] + "," \
        + ADC_Value_str[11] + "," \
        + ADC_Value_str[12] + "," \
        + ADC_Value_str[13] + "," \
        + ADC_Value_str[14] + "," \
        + ADC_Value_str[15] + "," \
        + DST_Value_str[0] + "," \
        + "\n")
    print(csvData)

    myDate = ("{0}{1:02d}{2:02d}".format(dt_now.year, dt_now.month, dt_now.day))

    #print(datetime.datetime.now())

    f_log = open('Indicator/data_log' + myDate + '.csv','a')
    f_tmp = open('Indicator/data.csv','w')
    #date_time = str(datetime.datetime.now())
    #date = (date_time[0:10])    
    #nowtime = (date_time[11:19])
    f_tmp.write(csvData)
    f_tmp.close()
    f_log.write(csvData)
    f_log.close() 
       


 
if __name__ == '__main__':
    # 共有メモリの作成
    # Valueオブジェクトの生成
    TR_Error = Value('d', 1)
    PR_Error = Value('d', 1)
    ADC_Error = Value('d', 1)
    DST_Error = Value('d', 1)

    # Arrayオブジェクトの生成
    TR_Value = Array('d', 4)
    PR_Value = Array('d', 8)
    ADC_Value = Array('d', 16)
    DST_Value = Array('d', 2)

    # プロセス定義
    p1 = Process(target=MainThread, args=[TR_Value, PR_Value, ADC_Value, DST_Value, TR_Error, PR_Error, ADC_Error, DST_Error])
    p2 = Process(target=StartPulseReaderThread, args=[PR_Value, PR_Error])
    p3 = Process(target=StartADCReaderThread, args=[ADC_Value, ADC_Error])
    p4 = Process(target=StartTemperatureReaderThread, args=[TR_Value, TR_Error])
    p5 = Process(target=StartDistanceReaderThread, args=[DST_Value, DST_Error])

    # プロセス実行
    p1.start()
    p2.start()
    p3.start()
    p4.start()
    p5.start()

signal.signal(signal.SIGALRM, OutputData)
signal.setitimer(signal.ITIMER_REAL, 0.1, 0.1)  #(,取得開始,記録周期)