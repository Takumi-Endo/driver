#coding: UTF-8
import datetime
import threading
import signal
from multiprocessing import Value, Array, Process
import time
import math
import RPi.GPIO as GPIO
#import psutil
import os
 
#libraly for Sensors and Config 
import ConfigApp as config
import PulseReader as PR
import BMSVoltageReader as BMSV
import BMSCurrentReader as BMSI
import TemperatureReader as TR
import TemperatureD6T44Reader as TR2
import AltimeterReader as ALT
import SystemBMSReader as SysBMS

index = "Date,Time,Time[ms]," \
        "temp1[deg],temp2[deg],temp3[deg],temp4[deg]," \
        "N1[rpm],N2[rpm],N3[rpm],N4[rpm],N5[rpm],N6[rpm],N7[rpm],N8[rpm]," \
        "V1[V],V2[V],V3[V],V4[V],V5[V],V6[V],V7[V],V8[V]," \
        "I1[A],I2[A],I3[A],I4[A],I5[A],I6[A],I7[A],I8[A]," \
        "h1[m],h2[m],h3[m],H[m],"\
        "SysV[V],SysI[A],\n"

TR_Value_tmp = [0 for i in range(4)]
TR2_Value_tmp = [0 for i in range(2)]
PR_Value_tmp = [0 for i in range(8)]
BMSV_Value_tmp = [0 for i in range(8)]
BMSI_Value_tmp = [0 for i in range(16)]
ALT_Value_tmp = [0 for i in range(8)]
SysBMS_Value_tmp = [0 for i in range(4)]

import debug
if debug.DEBUG:
    import random
    print("debug")

else:
    #GPIO.setmode(GPIO.BCM)
    #GPIO.cleanup()
    #GPIO.setup(4, GPIO.IN)

    #LED
    LED_G = 18
    LED_Y = 25
    LED_R = 26
    GPIO.setup(LED_G, GPIO.OUT, initial=GPIO.HIGH)
    GPIO.setup(LED_Y, GPIO.OUT, initial=GPIO.HIGH)
    GPIO.setup(LED_R, GPIO.OUT, initial=GPIO.HIGH)

#--------------------------------------------------
def StartPulseReaderThread():#(PR_Value):
    PR.GetValues(PR_Value)

def StartBMSVoltageReaderThread():#(BMSV_Value):
    #while True:
    BMSV.GetValues(BMSV_Value)
    #print(BMSV_Value)
    t=threading.Timer(0.099, StartBMSVoltageReaderThread)
    t.start()

def StartBMSCurrentReaderThread():#(BMSI_Value):
    #while True:
    BMSI.GetValues(BMSI_Value)
    t=threading.Timer(0.099,  StartBMSCurrentReaderThread)
    t.start()

def StartTemperatureReaderThread():#(TR_Value):
    #while True:
    TR.GetValues(TR_Value)
    t=threading.Timer(0.5, StartTemperatureReaderThread)
    t.start()

def StartTemperature2ReaderThread():#(TR2_Value):
    #while True:
    TR2.GetValues(TR2_Value)
    t=threading.Timer(1, StartTemperature2ReaderThread)
    t.start()

def StartAltimeterReaderThread():#(ALT_Value):
    #while True:
    ALT.GetValues(ALT_Value)
    t=threading.Timer(0.099, StartAltimeterReaderThread)
    t.start()

def StartSystemBMSReaderThread():#(SysBMS_Value):
    #while True:
    SysBMS.GetValues(SysBMS_Value)
    t=threading.Timer(0.099, StartSystemBMSReaderThread)#after tested, Timer value may be changed -> 1sec
    t.start()

def WriteLogData(arg1, arg2):
    
    try:
        dt_now = datetime.datetime.now()

        Time_Value = ("{0}-{1:02d}-{2:02d},{3:02d}:{4:02d}:{5:02d},{6:03d}" \
            .format(dt_now.year, dt_now.month, dt_now.day, \
                    dt_now.hour, dt_now.minute, dt_now.second, \
                    math.floor(dt_now.microsecond)))#/100000)*100)) #dt_now.microsecond))
        if debug.DEBUG:
            for i in range(8):
                if i<4:
                    TR_Value[i]=random.randint(30,35)
                    
                PR_Value[i]=random.randint(1900, 2150)
                ADC_Value[i]=random.randint(95,105)#Current
                ADC_Value[8+i]=random.randint(180,220)#Voltage
        
        #Temperature -----------------
        for i in range(len(TR_Value_tmp)):
            if TR_Value[i] == -9999:
                TR_Value_tmp[i] = "n/a"
            else:
                TR_Value_tmp[i] = str(TR_Value[i])

        for i in range(len(TR2_Value_tmp)):
            TR2_Value_tmp[i] = str(TR2_Value[i])

        #Motor Rotation --------------        
        for i in range(len(PR_Value_tmp)):
            PR_Value_tmp[i] = str(int(PR_Value[i]))
        
        #Main Battery Voltage --------
        for i in range(len(BMSV_Value_tmp)):
            if BMSV_Value[i] == -9999:
                BMSV_Value_tmp[i] = "n/a"
            else:
                BMSV_Value_tmp[i] = str(BMSV_Value[i])
            
        #Battery Output Current-------
        for i in range(len(BMSI_Value_tmp)):
            if BMSI_Value[i] == -9999:
                BMSI_Value_tmp[i] = "n/a"
            else:
                BMSI_Value_tmp[i] = str(BMSI_Value[i])
        
        #Altimeter -------------------
        for i in range(len(ALT_Value_tmp)):
            if ALT_Value[i] == -9999:
                ALT_Value_tmp[i] = "n/a"
            else:
                ALT_Value_tmp[i] = str(ALT_Value[i])
        
        #BMS for System Power --------
        for i in range(len(SysBMS_Value_tmp)):
            if SysBMS_Value[i] == -9999:
                SysBMS_Value_tmp[i] = "n/a"
            else:
                SysBMS_Value_tmp[i] = str(SysBMS_Value[i])
            
        csvData = (Time_Value + "," \
                + TR2_Value_tmp[0] + "," \
                + TR2_Value_tmp[1] + "," \
                + TR_Value_tmp[2] + "," \
                + TR_Value_tmp[3] + "," \
                + PR_Value_tmp[7] + "," \
                + PR_Value_tmp[1] + "," \
                + PR_Value_tmp[5] + "," \
                + PR_Value_tmp[3] + "," \
                + PR_Value_tmp[4] + "," \
                + PR_Value_tmp[2] + "," \
                + PR_Value_tmp[6] + "," \
                + PR_Value_tmp[0] + "," \
                + BMSV_Value_tmp[0] + "," \
                + BMSV_Value_tmp[1] + "," \
                + BMSV_Value_tmp[2] + "," \
                + BMSV_Value_tmp[3] + "," \
                + BMSV_Value_tmp[4] + "," \
                + BMSV_Value_tmp[5] + "," \
                + BMSV_Value_tmp[6] + "," \
                + BMSV_Value_tmp[7] + "," \
                + BMSI_Value_tmp[0] + "," \
                + BMSI_Value_tmp[2] + "," \
                + BMSI_Value_tmp[3] + "," \
                + BMSI_Value_tmp[4] + "," \
                + BMSI_Value_tmp[5] + "," \
                + BMSI_Value_tmp[6] + "," \
                + BMSI_Value_tmp[7] + "," \
                + BMSI_Value_tmp[1] + "," \
                + ALT_Value_tmp[0] + "," \
                + ALT_Value_tmp[1] + "," \
                + ALT_Value_tmp[2] + "," \
                + ALT_Value_tmp[5] + "," \
                + SysBMS_Value_tmp[0] + "," \
                + SysBMS_Value_tmp[1] + "," \
                + "\n")
    
        if not debug.DEBUG:
            print(csvData)

        myDate = ("{0}{1:02d}{2:02d}".format(dt_now.year, dt_now.month, dt_now.day))

    
        #if the file not found, create a new file and the first row must be written index.
        if not os.path.exists('Indicator/data_log' + myDate + '.csv'):
            f_log = open('Indicator/data_log' + myDate + '.csv','a')
            f_log.write(index)
        else:
            f_log = open('Indicator/data_log' + myDate + '.csv','a')
        
        f_tmp = open('../disp/data/data.csv','w')
        f_tmp.write(csvData)
        f_log.write(csvData)
    
    except KeyboardInterrupt:
        GPIO.output(LED_G, GPIO.LOW)
        GPIO.output(LED_Y, GPIO.LOW)
        GPIO.output(LED_R, GPIO.LOW)
    finally:
        f_tmp.close()
        f_log.close()

    #t=threading.Timer(0.099, WriteLogData) #signal.setitimer is better
    #t.start()

def SendData():#(arg1, arg2):
    #this area for IM920s
    #Altitude, BMS Voltage(min), BMS Current(max), Motor Rotation(max), Temperature(max)
    s = str(ALT_Value[5]) + "," + \
        str(min(BMSV_Value)) + "," + \
        str(max(BMSI_Value)) + "," + \
        str(int(max(PR_Value))) + "," + \
        str(max(TR_Value)) + "," 
    
    #IM920(s)
    #print(s)
    t=threading.Timer(0.1, SendData)
    t.start()

def LEDIndicator():
    GPIO.output(LED_G, GPIO.LOW)
    GPIO.output(LED_Y, GPIO.LOW)
    GPIO.output(LED_R, GPIO.LOW)
    time.sleep(1)
    
    if SysBMS_Value[3] == -9999:
        pass
    else:
        BTpct = SysBMS_Value[3]
        if BTpct > 50:
            #print("GREEN")
            GPIO.output(LED_G, GPIO.HIGH)
            GPIO.output(LED_Y, GPIO.LOW)
            GPIO.output(LED_R, GPIO.LOW)
        elif BTpct > 25:
            #print("YELLOW")
            GPIO.output(LED_G, GPIO.LOW)
            GPIO.output(LED_Y, GPIO.HIGH)
            GPIO.output(LED_R, GPIO.LOW)
        else:
            #print("RED")
            GPIO.output(LED_G, GPIO.LOW)
            GPIO.output(LED_Y, GPIO.LOW)
            GPIO.output(LED_R, GPIO.HIGH)
    
    t=threading.Timer(1, LEDIndicator)
    t.start()


if __name__ == '__main__':
    # Arrayオブジェクトの生成
    TR_Value = Array('d', 4)
    PR_Value = Array('d', 8)
    BMSV_Value = Array('d', 8)
    BMSI_Value = Array('d', 16)
    ALT_Value = Array('d', 8)
    SysBMS_Value = Array('d', 4)
    TR2_Value = Array('d', 2)
  
    #t1 = threading.Thread(target=StartPulseReaderThread)
    t2 = threading.Thread(target=StartPulseReaderThread)#, args=[PR_Value])
    t3 = threading.Thread(target=StartBMSVoltageReaderThread)#, args=[BMSV_Value])
    t4 = threading.Thread(target=StartBMSCurrentReaderThread)#, args=[BMSI_Value])
    t5 = threading.Thread(target=StartTemperatureReaderThread)#, args=[TR_Value])
    t6 = threading.Thread(target=StartTemperature2ReaderThread)#, args=[TR2_Value])
    t7 = threading.Thread(target=StartAltimeterReaderThread)#, args=[ALT_Value])
    t8 = threading.Thread(target=StartSystemBMSReaderThread)#, args=[SysBMS_Value])
    #t9 = threading.Thread(target=WriteLogData)#, args=[PR_Value, BMSV_Value])
    t10 = threading.Thread(target=SendData)
    t11 = threading.Thread(target=LEDIndicator)

    #t1.start()
    t2.start()
    t3.start()
    t4.start()
    t5.start()
    t6.start()
    t7.start()
    t8.start()
    #t9.start()
    t10.start()
    t11.start()

signal.signal(signal.SIGALRM, WriteLogData)
signal.setitimer(signal.ITIMER_REAL, 1, 0.1)  #(,取得開始,記録周期)