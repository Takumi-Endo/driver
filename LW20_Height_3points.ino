//-------------------------------------------------------------------------------------------
// LightWare Arduino I2C connection sample
// https://lightware.co.za
//-------------------------------------------------------------------------------------------
// Compatible with the following devices:
// - SF02
// - SF10
// - SF11
// - LW20/SF20
//-------------------------------------------------------------------------------------------

#include <Wire.h>
#include <Math.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

int i, x1, x2, c1, c2, c3;
int Addr[] = {0x61, 0x62, 0x63};

void setup()
{
  Wire.begin();
  Serial.begin(115200);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);

  x1 = 11;  // [cm] Distance between Sensor 1 and 2
  x2 = 32; // [cm] Distance between calculated 1-2 and Sensor 3

  c1 = 0; // [cm] calibration
  c2 = 3; // [cm] calibration
  c3 = -1; // [cm] calibration
    
}

void loop()
{
  int L = MeasureHeight();
  //DisplayOut(L);
  delay(500);
}

float MeasureHeight()
{
  int distance1, distance2, distance3;
  int disTmp1, disTmp2;
  float Deg1, Deg2;
  float La, Lb, L, L1, L2, L12;
  
  Wire.requestFrom(0x61, 2);
  if (Wire.available() >= 2){
    int tmp1H = Wire.read();
    int tmp1L = Wire.read();
    distance1 = tmp1H * 256 + tmp1L + c1;
  }
  Serial.print(distance1);
  Serial.print(" cm, ");

  Wire.requestFrom(0x62, 2);
  if (Wire.available() >= 2){
    int tmp2H = Wire.read();
    int tmp2L = Wire.read();
    distance2 = tmp2H * 256 + tmp2L + c2;
    }
  Serial.print(distance2);
  Serial.print(" cm, ");
  
  Wire.requestFrom(0x63, 2);
  if (Wire.available() >= 2){
    int tmp3H = Wire.read();
    int tmp3L = Wire.read();
    distance3 = tmp3H * 256 + tmp3L + c3;
  }
  Serial.print(distance3);
  Serial.print(" cm, ");
  Serial.println();

  
  //if distance1 <> 0 and distance2 <> 0 and distance3 <> 0{

  //-----------------------------
  La = abs(distance1 - distance2);
  Deg1 = degrees(atan(La / x1));
  if (distance1 < distance2) {
    disTmp1 = distance1;
  } else {
    disTmp1 = distance2;
  }
  
  L12 = ( disTmp1 + (La/2) ) * cos(radians(Deg1));

  //-----------------------------
  Lb = abs(distance3 - L12);
  Deg2 = degrees(atan(Lb / x2));
  if (L12 < distance3) {
    disTmp2 = L12;
  } else {
    disTmp2 = distance3;
  }

  Serial.print(Deg1);
  Serial.print("deg ");
  Serial.print(Deg2);
  Serial.println("deg");

  int result = (disTmp2 + (Lb/2)) * cos(radians(Deg2));
  Serial.print( result );
  Serial.println(" cm, ");

  //display out
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.print(distance1);
  display.print("cm ");
  display.print(distance2);
  display.print("cm ");
  display.print(distance3);
  display.println("cm");
  display.print(Deg1);
  display.print("deg ");
  display.print(Deg2);
  display.println("deg");

  display.setTextSize(2);
  display.print( result );
  display.print(" cm");
  display.display();




  
  return result;
}

void DisplayOut(int DataOut)
{
  //display out
  display.clearDisplay();
  display.setTextSize(3);
  display.setTextColor(WHITE);
  
  display.setCursor(0, 0); 
  display.print( DataOut );
  display.print(" cm");
  display.display();
}

// やぐらの高さ4740mm
