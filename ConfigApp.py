#!/usr/bin/env python
# -*- coding: utf-8 -*-
#Settings
 
#パルス取得関連
SmpPulseSec = 0.1             #パルス取得間隔　sec
WaitPulse = 0.0002          #パルス取得待ち時間 だいたい####回/秒
 
#GPIO settings
GPIO_P0 = 5                #GPIO #
GPIO_P1 = 6                
GPIO_P2 = 13               
GPIO_P3 = 19
GPIO_P4 = 12
GPIO_P5 = 16
GPIO_P6 = 20
GPIO_P7 = 21