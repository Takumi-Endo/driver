#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import datetime
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

#global ADC_Value

# Create the I2C bus
i2c = busio.I2C(board.SCL, board.SDA, frequency=3400000)

# Create the ADC object using the I2C bus
ads1 = ADS.ADS1115(i2c, address=0x48, gain=2/3)
ads2 = ADS.ADS1115(i2c, address=0x49, gain=2/3)
ads3 = ADS.ADS1115(i2c, address=0x4a, gain=2/3)
ads4 = ADS.ADS1115(i2c, address=0x4b, gain=2/3)
ads5 = ADS.ADS1115(i2c, address=0x4c, gain=2/3)
ads6 = ADS.ADS1115(i2c, address=0x4d, gain=2/3)
ads7 = ADS.ADS1115(i2c, address=0x4e, gain=2/3)
ads8 = ADS.ADS1115(i2c, address=0x4f, gain=2/3)

Gain_V1 = 22.6222	#22.886	#27 #要再調整
Gain_V2 = 22.5886
Gain_V3 = 22.7434
Gain_V4 = 22.5658
Gain_V5 = 22.7920
Gain_V6 = 22.5996
Gain_V7 = 22.5706
Gain_V8 = 22.6804
Gain_A = 50 #要再調整

def StartADCReader(ADC_Value):
	# Create single-ended input on channel 0
	#print(datetime.datetime.now())
	ch11 = AnalogIn(ads1, ADS.P0).voltage
	ch12 = AnalogIn(ads1, ADS.P1).voltage
	ch13 = AnalogIn(ads1, ADS.P2).voltage
	ch14 = AnalogIn(ads1, ADS.P3).voltage
	ch21 = AnalogIn(ads2, ADS.P0).voltage
	ch22 = AnalogIn(ads2, ADS.P1).voltage
	ch23 = AnalogIn(ads2, ADS.P2).voltage
	ch24 = AnalogIn(ads2, ADS.P3).voltage
	ch31 = 0	#AnalogIn(ads3, ADS.P0).voltage
	ch32 = 0	#AnalogIn(ads3, ADS.P1).voltage
	ch33 = 0	#AnalogIn(ads3, ADS.P2).voltage
	ch34 = 0	#AnalogIn(ads3, ADS.P3).voltage
	ch41 = 0	#AnalogIn(ads4, ADS.P0).voltage
	ch42 = 0	#AnalogIn(ads4, ADS.P1).voltage
	ch43 = 0	#AnalogIn(ads4, ADS.P2).voltage
	ch44 = 0	#AnalogIn(ads4, ADS.P3).voltage
	ch51 = AnalogIn(ads5, ADS.P0).voltage
	ch52 = AnalogIn(ads5, ADS.P1).voltage
	ch53 = AnalogIn(ads5, ADS.P2).voltage
	ch54 = AnalogIn(ads5, ADS.P3).voltage
	ch61 = AnalogIn(ads6, ADS.P0).voltage
	ch62 = AnalogIn(ads6, ADS.P1).voltage
	ch63 = AnalogIn(ads6, ADS.P2).voltage
	ch64 = AnalogIn(ads6, ADS.P3).voltage
	ch71 = 0	#AnalogIn(ads7, ADS.P0).voltage
	ch72 = 0	#AnalogIn(ads7, ADS.P1).voltage
	ch73 = 0	#AnalogIn(ads7, ADS.P2).voltage
	ch74 = 0	#AnalogIn(ads7, ADS.P3).voltage
	ch81 = 0	#AnalogIn(ads8, ADS.P0).voltage
	ch82 = 0	#AnalogIn(ads8, ADS.P1).voltage
	ch83 = 0	#AnalogIn(ads8, ADS.P2).voltage
	ch84 = 0	#AnalogIn(ads8, ADS.P3).voltage
	
	#print(datetime.datetime.now())

	val11 = round(ch12 * Gain_V1, 1)
	val12 = round(ch14 * Gain_A, 0)
	val13 = 0
	val14 = 0
	val21 = round(ch22 * Gain_V2, 1)
	val22 = round(ch24 * Gain_A, 0)
	val23 = 0
	val24 = 0
	val31 = round(ch62 * Gain_V3, 1)
	val32 = round(ch64 * Gain_A, 0)
	val33 = 0
	val34 = 0
	val41 = round(ch52 * Gain_V4, 1)
	val42 = round(ch54 * Gain_A, 0)
	val43 = 0
	val44 = 0
	val51 = round(ch21 * Gain_V5, 1)
	val52 = round(ch23 * Gain_A, 0)
	val53 = 0
	val54 = 0
	val61 = round(ch11 * Gain_V6, 1)
	val62 = round(ch13 * Gain_A, 0)
	val63 = 0
	val64 = 0
	val71 = round(ch51 * Gain_V7, 1)
	val72 = round(ch53 * Gain_A, 0)
	val73 = 0
	val74 = 0
	val81 = round(ch61 * Gain_V8, 1)
	val82 = round(ch63 * Gain_A, 0)
	val83 = 0
	val84 = 0
	ADC_Value[0] = val11
	ADC_Value[1] = val21
	ADC_Value[2] = val31
	ADC_Value[3] = val41
	ADC_Value[4] = val51
	ADC_Value[5] = val61
	ADC_Value[6] = val71
	ADC_Value[7] = val81
	ADC_Value[8] = val12
	ADC_Value[9] = val22
	ADC_Value[10] = val32
	ADC_Value[11] = val42
	ADC_Value[12] = val52
	ADC_Value[13] = val62
	ADC_Value[14] = val72
	ADC_Value[15] = val82


	#print(datetime.datetime.now())
    
	# Create differential input between channel 0 and 1
	#chan = AnalogIn(ads, ADS.P0, ADS.P1)

	#print("{:>5}\t{:>5}".format('raw', 'v'))
	
	#print("{:>5}\t{:>5}\t{:>5}\t{:>5}\t{:>5}\t{:>5}\t{:>5}\t{:>5}". \
	#	format('V1', 'V2', 'V3', 'V4', 'V5', 'V6', 'V7', 'V8'))

	#while True:
	    #print("{:>5}\t{:>5.3f}".format(chan1.value, chan1.voltage))#, chan2.value, chan2,voltage))
	    #print("{:>5}\t{:>5.3f}".format(chan2.value, chan2.voltage))
	    #print("{:>5}\t{:>5.3f}\t{:>5}\t{:>5.3f}".format(chan1.value, chan1.voltage, chan2.value, chan2.voltage))
		#print("{:>5.3f}\t{:>5.3f}\t{:>5.3f}\t{:>5.3f}\t{:>5.3f}\t{:>5.3f}\t{:>5.3f}\t{:>5.3f}" \
		#	.format(ch11, ch12, ch13, ch14, ch21, ch22, ch23, ch24))
		#time.sleep(0.1)
	# ADC_Value
	#myData = ("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15}" \
	#		.format(val11, val21, val31, val41, val51, val61, val71, val81, \
	#				val12, val22, val32, val42, val52, val62, val72, val82))



	#print(datetime.datetime.now())

#	return myData