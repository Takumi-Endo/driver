#coding: UTF-8
import datetime
import time
import math
import RPi.GPIO as GPIO

import ConfigApp as config

ch = 8

gpio_p = [0 for i in range(ch)]
cntP = [0 for i in range(ch)]
bPo = [0 for i in range(ch)]
bP = [0 for i in range(ch)]
myValue = [0 for i in range(ch)]

#設定取得
GPIO.setmode(GPIO.BCM)                  #GPIO利用設定
#GPIO.cleanup()
for i in range(ch):
    GPIO.setup(eval("config.GPIO_P"+str(i)), GPIO.IN)
    gpio_p[i] = eval("config.GPIO_P"+str(i))

waitpulse = config.WaitPulse
smpsec = config.SmpPulseSec

calc1 = 2 * 8   #on/off * テープの数 of motor rotation
calc2 = 60 / smpsec #calculate for rpm

# パルス取得ルーチン
def GetValues(myValue):
#while True:
    #for i in range(1,ch):
    #    bPo[i] = GPIO.input(gpio_p[i])                 #GPIO状態

    while True:
        ftime_e = time.time() + smpsec

        cnt = 0             #ループ回数
        for i in range(ch): cntP[i] = 0 #パルス変化回数
 
        while time.time() <= ftime_e:
            #counting pulse
            for i in range(ch):
                bP[i] = GPIO.input(gpio_p[i])
                if bPo[i] != bP[i]:
                    cntP[i] += 1
                    bPo[i] = bP[i]
                    
            cnt += 1
#            time.sleep(waitpulse)

        #Output
        for i in range(ch):
            myValue[i] = math.floor(cntP[i] / calc1 * calc2)