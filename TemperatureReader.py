#!/usr/bin/env python
# -*- coding: utf-8 -*-
import smbus
import time

bus = smbus.SMBus(1)
i2cHeadAddr = 0x48
i2c_ch = 4 #mount of i2c channel
i2c_addr = [i2cHeadAddr+i for i in range(i2c_ch)]#[0x48, 0x49, 0x4a, 0x4b]
i2c_exist = [False for i in range(i2c_ch)]#[False, False, False, False]

register_adt7410 = 0x00
configration_adt7410 = 0x03

myValue_buf = []
myValue = [0 for i in range(i2c_ch)]

def GetValues(myValue):
#while True:
    # INITIAL SETTINGS ------
    for i in range(len(i2c_addr)):
        if i2c_exist[i] == False:
            try:
                bus.write_word_data(i2c_addr[i], configration_adt7410, 0x80) # 16bitに設定
                i2c_exist[i] = True
            except IOError:
                #s = hex(i2c_addr[i]) + " does not exist."
                #print(s)
                i2c_exist[i] = False

    # READ ------------------
    for i in range(len(i2c_addr)):
        try:
            buf = bus.read_word_data(i2c_addr[i], register_adt7410)
            buf = (buf & 0xff00) >> 8 | (buf & 0xff) << 8
            buf = (round(buf / 128, 1))
        except IOError:
            buf = -9999#"n/a"
        
        myValue_buf.append(buf)
    
    # OUTPUT ----------------
    for i in range(i2c_ch):#len(i2c_addr)):
        myValue[i] = myValue_buf[i]
    #print(myValue)
    myValue_buf.clear()
    #myValue.clear()
    
    #time.sleep(0.1)
