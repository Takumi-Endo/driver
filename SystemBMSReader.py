import smbus
import time

i2c = smbus.SMBus(1)
i2c_addr = [0x4f]
buf = []
#bufBR = 0
myValue = [0 for i in range(4)]

#Battery Voltage Max and Min
MaxV = 12.4
MinV = 3.7*3

def GetValues(myValue):
#while True:
    # READ ------------------
    for i in range(len(i2c_addr)):
        try:
            #Voltage
            bufV = i2c.read_word_data(i2c_addr[i], 0x02)  #read sensor Voltage value
            bufV = ( (bufV & 0xff00) >> 8 | (bufV & 0xff) << 8 )
            bufV = round( bufV*1.25/1000, 2)
            
            #Current
            bufI = i2c.read_word_data(i2c_addr[i], 0x01) #read sensor Current value
            bufI = ( (bufI & 0xff00) >> 8 | (bufI & 0xff) << 8 )
            bufI = round( bufI*1.25/1000, 2)
            
            bufP = bufV * bufI
            
            bufBR = ((bufV - MinV) / (MaxV - MinV)) * 100
            if bufBR > 100: bufBR = 100
            if bufBR < 0: bufBR = 0

        except IOError: #sensor error
            bufV = -9999
            bufI = -9999
            bufP = -9999
            bufBR = -9999
        
        buf.append(bufV)
        buf.append(bufI)
        buf.append(bufP)
        buf.append(bufBR)
    
    # OUTPUT ----------------
    for i in range(len(buf)):
        myValue[i] = buf[i]

    #print(myValue)
    buf.clear()    
    
    #time.sleep(0.1)
